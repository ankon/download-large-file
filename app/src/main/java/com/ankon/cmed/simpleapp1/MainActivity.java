package com.ankon.cmed.simpleapp1;

import android.Manifest;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.icu.text.DecimalFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    public final int REQUEST_WRITE_EXTERNAL_STORAGE = 1024;

    Context context;
    DownloadManager downloadManager;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Runnable runnable;
    Handler handler;

    TextView downloadProgress;

    private String url = "http://dropbox.sandbox2000.com/intrvw/SampleVideo_1280x720_30mb.mp4";
    private long downloadId;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
        downloadManager = (DownloadManager)getSystemService(Context.DOWNLOAD_SERVICE);

        initializeProgressDialog();
        initializeTimer();
        initializeDownloadId();

        ((Button) findViewById(R.id.button_download)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);

                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_STORAGE);
                } else {
                    startDownload();
                }
            }
        });

        runnable.run();
    }

    private void initializeTimer() {
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    showDownloadProgress();
                } finally {
                    handler.postDelayed(runnable, 500);
                }
            }
        };
    }

    private void initializeProgressDialog() {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Downloading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
    }

    private void initializeDownloadId() {
        sharedPreferences = getSharedPreferences("SimpleApp1.conf", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        if (sharedPreferences.getLong("download_id", -1) == -1) {
            editor.putLong("download_id", 0);
            editor.apply();
            downloadId = 0;
        } else {
            downloadId = sharedPreferences.getLong("download_id", 0);
        }

        downloadProgress = (TextView) findViewById(R.id.tv_download_progress);
        downloadProgress.setText("");
    }

    private void startDownload() {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription("Download in progress...");
        request.setTitle("Downloading File!");
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "SampleVideo_1280x720_30mb.mp4");

        downloadId = downloadManager.enqueue(request);
        editor.putLong("download_id", downloadId);
        editor.apply();
    }

    private void showDownloadProgress() {
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(downloadId);

        Cursor c = downloadManager.query(query);
        if (c.moveToFirst()) {
            int sizeIndex = c.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES);
            int downloadedIndex = c.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR);
            long size = c.getInt(sizeIndex);
            long downloaded = c.getInt(downloadedIndex);
            double progress = 0.0;
            if (size != -1) progress = downloaded * 100.0/size;

            downloadProgress.setText((Math.round(progress * 100) / 100) + "" + getResources().getString(R.string.percentage));
            progressDialog.setProgress((int) progress);

            progressDialog.show();
            if (progress >= 100 || progress <= 0) progressDialog.cancel();
        } else progressDialog.cancel();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_WRITE_EXTERNAL_STORAGE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    startDownload();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        handler.removeCallbacks(runnable);
    }
}
